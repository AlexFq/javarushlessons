package com.javarush.test.level04.lesson06.task03;

/* Сортировка трех чисел
Ввести с клавиатуры три числа, и вывести их в порядке убывания.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        int[] ints  = new int[3];
//        ints[0] = Integer.parseInt( reader.readLine());
//        ints[1] = Integer.parseInt( reader.readLine());
//        ints[2] = Integer.parseInt( reader.readLine());
//        ints =  simpleSort(ints);
        int[] ints = {5,6,3,9,1};
        ints = bubbleSort(ints);
        printIntArr(ints);

        //напишите тут ваш код

    }
    static int[] simpleSort(int ints[])
    {
        int i = 0;
        int[] result = ints;

        while (true)
        {
            if (i == ints.length - 1) break;

            if (result[i] < result[i + 1])
            {
                result = swap(ints, i, i + 1);
                i= 0;
                continue;
            }
            i++;
        }
        return result;
    }

    static int[] bubbleSort(int ints[])
    {
        int[] result = ints;
//        printIntArr(result);
        for (int i = 0; i < result.length - 1; i++)
            for (int j = 0; j < result.length - 1; j++)
                if (result[j] > result[j + 1])
                {
                    result = swap(result, j, j + 1);
//                    printIntArr(result);
                }
        return result;
    }

    private static int[] swap(int[] ints, int i, int i1)
    {
        int[] result = ints;
        result[i] =result[i] + result[i1];
        result[i1] = result[i] - result[i1];
        result[i] = result[i] - result[i1];
        return result;
    }

    private static void printIntArr(int[] ints)
    {
        for (int anInt : ints)
        {
            System.out.print(anInt);
        }
        System.out.println();
    }
}
