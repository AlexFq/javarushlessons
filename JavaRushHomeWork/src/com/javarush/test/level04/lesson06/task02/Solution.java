package com.javarush.test.level04.lesson06.task02;

/* Максимум четырех чисел
Ввести с клавиатуры четыре числа, и вывести максимальное из них.
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Solution solution = new Solution();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());
        int d = Integer.parseInt(reader.readLine());
        System.out.println(solution.max4(a,b,c,d));
    }

    int max4(int a, int b, int c, int d)
    {
        if (max(a,b) > max(c,d)) return max(a,b);
        else return max(c,d);
    }
    int max(int a, int b)
    {
        if (a>b) return a;
        else  return b;

    }
}
