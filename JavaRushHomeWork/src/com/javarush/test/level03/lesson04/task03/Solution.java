package com.javarush.test.level03.lesson04.task03;

/* StarCraft
Создать 10 зергов, 5 протосов и 12 терран.
Дать им всем уникальные имена.
*/

public class Solution
{
    public static void main(String[] args)
    {
        Zerg zerg,zerg1,zerg2,zerg3,zerg4,zerg5,zerg6,zerg7,zerg8,zerg9;
        Protos protos,protos1,protos2,protos3,protos4;
        Terran terran,terran1,terran2,terran3,terran4,terran5,terran6,terran7,terran8,terran9,terran10,terran11;

        zerg = new Zerg();
        zerg1 = new Zerg();
        zerg2 = new Zerg();
        zerg3 = new Zerg();
        zerg4 = new Zerg();
        zerg5 = new Zerg();
        zerg6 = new Zerg();
        zerg7 = new Zerg();
        zerg8 = new Zerg();
        zerg9 = new Zerg();

        protos = new Protos();
        protos1 = new Protos();
        protos2 = new Protos();
        protos3 = new Protos();
        protos4 = new Protos();

        terran = new Terran();
        terran1 = new Terran();
        terran2 = new Terran();
        terran3 = new Terran();
        terran4 = new Terran();
        terran5 = new Terran();
        terran6 = new Terran();
        terran7 = new Terran();
        terran8 = new Terran();
        terran9 = new Terran();
        terran10 = new Terran();
        terran11 = new Terran();

        zerg.name = "z1";
        zerg1.name = "z2";
        zerg2.name = "z3";
        zerg3.name = "z4";
        zerg4.name = "z5";
        zerg5.name = "z6";
        zerg6.name = "z7";
        zerg7.name = "z8";
        zerg8.name = "z9";
        zerg9.name = "z10";

        protos.name = "p1";
        protos1.name = "p2";
        protos2.name = "p3";
        protos3.name = "p4";
        protos4.name = "p5";

        terran.name = "t1";
        terran1.name = "t2";
        terran2.name = "t3";
        terran3.name = "t4";
        terran4.name = "t5";
        terran5.name = "t6";
        terran6.name = "t7";
        terran7.name = "t8";
        terran8.name = "t9";
        terran9.name = "t10";
        terran10.name = "t11";
        terran11.name = "t12";


    }

    public static class Zerg
    {
        public String name;
    }

    public static class Protos
    {
        public String name;
    }

    public static class Terran
    {
        public String name;
    }
}