package com.javarush.test.level03.lesson04.task01;

/* Дата рождения
Вывести на экран дату своего рождения в виде: MAY 1 2012
*/

import javax.xml.crypto.Data;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.StreamHandler;

public class Solution
{
    public static void main(String[] args) throws ParseException
    {
        //напишите тут ваш код
        String myBirsday = "19.04.1993";
        Date oldData = new SimpleDateFormat("dd.MM.yyyy",Locale.ENGLISH).parse(myBirsday);
        String newDate = new SimpleDateFormat("MMMM dd yyyy",Locale.ENGLISH).format(oldData);
        newDate = newDate.toUpperCase();
        //System.out.println("APRIL 19 1993");
        System.out.println(newDate);
    }
}