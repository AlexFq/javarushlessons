package com.javarush.test.level03.lesson06.task01;

/* Мама мыла раму
Вывести на экран все возможные комбинации слов «Мама», «Мыла», «Раму».
Подсказка: их 6 штук. Каждую комбинацию вывести с новой строки. Слова не разделять. Пример:
МылаРамуМама
РамуМамаМыла
...
*/

import java.util.Objects;
import java.util.Scanner;

public class Solution
{
    public static void main(String[] args)
    {
        String strings = "";
        System.out.print("Введите слово:");
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNext())
        strings = scanner.next();
        System.out.println();
        Object[]chars = CharAToObjA(strings);
        printArray2(bustOfObject(chars));
//        String[] strings1 = {"Мама", "Мыла", "Раму"};
//        printArray2(bustOfObject(strings1));
    }

    private static Object[] CharAToObjA(String s)
    {
        Object[] objects = new Object[s.length()];
        char[] characters = s.toUpperCase().toCharArray();
        for (int i = 0; i < s.length(); i++)
            objects[i] = characters[i];
        return objects;
    }

    private static void printArray2(Object[][] objects)
    {
        for (Object[] object : objects)
        {
            for (Object o : object)
            {
                System.out.print(o + " ");
            }
            System.out.println();
        }
    }
    private static Object[][] bustOfObject(Object[] objects)
    {

        int bustCount = (int)factorial(objects.length); //Считаем количество вариантов
        System.out.println("Количество переборов: " + bustCount);
        Object[][] result = new Object[bustCount][objects.length];
        int matrixCount = bustCount / objects.length; //Считаем количесвто матриц
        Object[][] matrix = new Object[matrixCount][objects.length];//Создаем Матрицу ключевых переборов
        matrix[0] = objects.clone();//Входящя последовательность - первый ключевой перебор

        //Высчитавем все ключевые переборы
        int count = objects.length - 1;
        for (int i = 1; i < matrixCount; i++)
        {
            if (count == 1) count = objects.length - 1;
            Object o = objects[count - 1];
            objects[count - 1] = objects[count];
            objects[count] = o;
            matrix[i] = objects.clone();
            count--;
        }

        int topResult = 0;
        for (int i = 0; i < matrixCount; i++)
        {
            Object[][] matrixCurrent = new Object[objects.length][objects.length];
            matrixCurrent[0] = matrix[i].clone();
            for (int j = 1; j < objects.length; j++)
            {
                for (int k = 0; k < objects.length; k++)
                {
                    if (k != objects.length - 1) matrixCurrent[j][k] = matrixCurrent[j-1][k+1];
                    else matrixCurrent[j][k] = matrixCurrent[j-1][0];
                }
            }

            for (int j = 0; j < objects.length; j++)
            {
                result[topResult] = matrixCurrent[j];
                topResult++;
            }

        }
        return result;
    }//bustOfObject
    private static long factorial(int i)
    {
        long res = 1;
        for (int j = 1; j <= i; j++)
        {
            res *=  j;
        }
        return res;
    }
}